use dotenv::dotenv;
use hyper::{Body, Client, Method, Request};
use hyper_tls::HttpsConnector;
use serde::{Deserialize, Serialize};
use serde_json::{self};
use std::env;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let config = get_config();
    let dns_domain = get_dns_domain(&config.sub_domain, &config.domain);
    let ip = get_ip().await?;

    if let Some(zone_id) = get_zone_id(&config.domain, &config.email, &config.key).await? {
        if let Some(record) = get_subdomain(&zone_id, &config.email, &config.key, &dns_domain).await? {
            // exists, check address
            if record.content == ip {
                println!("{} is already {}, no need to update", dns_domain, ip);
            } else {
                // different, update teh address
                update_subdomain(&zone_id, &config.email, &config.key, &dns_domain, &record.id, &ip).await?;
                println!("Successfully updated {} with {}", dns_domain, ip);
            }
        } else {
            // create new entry
            create_subdomain(&zone_id, &config.email, &config.key, &dns_domain, &ip).await?;
            println!("Successfully created {} with {}", dns_domain, ip);
        }
    }

    Ok(())
}

#[derive(Debug)]
struct Config {
    email: String,
    key: String,
    domain: String,
    sub_domain: Option<String>,
}

fn get_config() -> Config {
    dotenv().ok();

    let mut config = Config { email: "".to_string(), key: "".to_string(), domain: "".to_string(), sub_domain: None };

    if let Ok(x) = env::var("EMAIL") {
        config.email = x.trim().to_string();
    }
    if let Ok(x) = env::var("KEY") {
        config.key = x.trim().to_string();
    }
    if let Ok(x) = env::var("DOMAIN") {
        config.domain = x.trim().to_string();
    }
    if let Ok(x) = env::var("SUB_DOMAIN") {
        let new = x.trim().to_string();
        if !new.is_empty() {
            config.sub_domain = Some(new);
        }
    }

    config
}

fn get_dns_domain(sub_domain: &Option<String>, domain: &String) -> String {
    if let Some(sub) = sub_domain {
        format!("{}.{}", sub, domain)
    } else {
        domain.to_string()
    }
}

async fn get_ip() -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);
    let uri = "https://checkip.amazonaws.com".parse()?;

    let resp = client.get(uri).await?;

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await?;
    Ok(String::from_utf8(body_bytes.to_vec())?.trim().to_string())
}

#[derive(Serialize, Deserialize)]
struct ZoneID {
    result: Vec<ZoneIdInner>,
}
#[derive(Serialize, Deserialize)]
struct ZoneIdInner {
    id: String,
}

async fn get_zone_id(domain: &String, email: &String, key: &String) -> Result<Option<String>, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let req = Request::builder()
        .method(Method::GET)
        .uri(&format!("https://api.cloudflare.com/client/v4/zones?name={}", domain))
        // headers
        .header("X-Auth-Email", email)
        .header("X-Auth-Key", key)
        .header("Content-Type", "application/json")
        // body
        .body(Body::from("Hallo"))?;

    let resp = client.request(req).await?;

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await?;

    let json = String::from_utf8(body_bytes.to_vec())?;
    let parsed: ZoneID = serde_json::from_str(&json)?;

    if !parsed.result.is_empty() {
        Ok(Some(parsed.result[0].id.to_owned()))
    } else {
        Ok(None)
    }
}

#[derive(Serialize, Deserialize)]
struct DnsRecords {
    result: Vec<DnsRecord>,
}
#[derive(Serialize, Deserialize, Clone, Debug)]
struct DnsRecord {
    id: String,
    content: String,
}

async fn get_subdomain(zone_id: &String, email: &String, key: &String, dns_domain: &String) -> Result<Option<DnsRecord>, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let req = Request::builder()
        .method(Method::GET)
        .uri(&format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records?type=A&name={}", zone_id, dns_domain))
        // headers
        .header("X-Auth-Email", email)
        .header("X-Auth-Key", key)
        .header("Content-Type", "application/json")
        // body
        .body(Body::from("Hallo"))?;

    let resp = client.request(req).await?;

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await?;

    let json = String::from_utf8(body_bytes.to_vec())?;
    let parsed: DnsRecords = serde_json::from_str(&json)?;

    if !parsed.result.is_empty() {
        Ok(Some(parsed.result[0].to_owned()))
    } else {
        Ok(None)
    }
}

async fn update_subdomain(zone_id: &String, email: &String, key: &String, dns_domain: &String, dns_id: &String, ip: &String) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let req = Request::builder()
        .method(Method::PUT)
        .uri(&format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}", zone_id, dns_id))
        // headers
        .header("X-Auth-Email", email)
        .header("X-Auth-Key", key)
        .header("Content-Type", "application/json")
        // body
        .body(Body::from(format!("{{\"type\":\"A\",\"name\":\"{}\",\"content\":\"{}\",\"ttl\":1,\"proxied\":false}}", dns_domain, ip)))?;

    client.request(req).await?;

    Ok(())
}

async fn create_subdomain(zone_id: &String, email: &String, key: &String, dns_domain: &String, ip: &String) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let req = Request::builder()
        .method(Method::POST)
        .uri(&format!("https://api.cloudflare.com/client/v4/zones/{}/dns_records", zone_id))
        // headers
        .header("X-Auth-Email", email)
        .header("X-Auth-Key", key)
        .header("Content-Type", "application/json")
        // body
        .body(Body::from(format!("{{\"type\":\"A\",\"name\":\"{}\",\"content\":\"{}\",\"ttl\":1,\"proxied\":false}}", dns_domain, ip)))?;

    client.request(req).await?;

    Ok(())
}
